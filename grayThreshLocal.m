function [ THRESHOLDS ] = grayThreshLocal(greyImage, debug )

if (strcmp(debug, 'debug'))
    display(greyImage);
end

% Wyznaczenie rozmiar�w obrazu
HEIGHT = size(greyImage, 1);
WIDTH = size(greyImage, 2);

% Wyznaczenie wielko�ci lokalnych kom�rek na jakie b�dzie dzielony obraz
LOCAL_CELL_HEIGHT = fix(HEIGHT / 10);
LOCAL_CELL_WIDTH = fix(WIDTH / 10);

% Wyznaczenie tablicy podzia��w w pionie - VERTICAL_DIVISIONS
VERTICAL_COUNT = fix(WIDTH / LOCAL_CELL_HEIGHT);
VERTICAL_REST = mod(WIDTH,LOCAL_CELL_HEIGHT);
VERTICAL_DIVISIONS(1:VERTICAL_COUNT) = LOCAL_CELL_HEIGHT;
if VERTICAL_REST ~= 0
    VERTICAL_DIVISIONS(VERTICAL_COUNT+1)=VERTICAL_REST;
end

if (strcmp(debug, 'debug'))
    display(VERTICAL_COUNT);
    display(VERTICAL_REST);
    display(VERTICAL_DIVISIONS);
end

% Wyznaczenie tablicy podzia��w w poziomie - HORIZONTAL_DIVISIONS
HORIZONTAL_COUNT = fix(HEIGHT / LOCAL_CELL_WIDTH);
HORIZONTAL_REST = mod(HEIGHT, LOCAL_CELL_WIDTH);
HORIZONTAL_DIVISIONS(1:HORIZONTAL_COUNT) = LOCAL_CELL_WIDTH;
if HORIZONTAL_REST ~= 0
    HORIZONTAL_DIVISIONS(HORIZONTAL_COUNT+1)=HORIZONTAL_REST;
end

if (strcmp(debug, 'debug'))
    display(HORIZONTAL_COUNT);
    display(HORIZONTAL_REST);
    display(HORIZONTAL_DIVISIONS);
end;

% Podzia� obrazu na kom�rki z kt�rych ka�da zawiera macierz lokaln�
CELLS = mat2cell(greyImage, HORIZONTAL_DIVISIONS, VERTICAL_DIVISIONS);

if (strcmp(debug, 'debug'))
    display(CELLS);
end;

% Wyznaczenie poziomu progowania dla ka�dej macierzy lokalnej algorytmem
% Otsu, a nast�pnie zamiana podmiana warto�ci na poziomy progowania
for i = 1:size(CELLS, 1)
    for j = 1:size(CELLS, 2)
        localMatrix = CELLS{i, j};
        localThreshold = graythresh(localMatrix);
        localMatrix = repmat(localThreshold, size(localMatrix));
        CELLS{i, j} = localMatrix;
    end
end

if (strcmp(debug, 'debug'))
    display(CELLS);
end;

% Pol�cznie macierzy kom�rek z progami w zwyk�� macierz prog�w
T = cell2mat(CELLS);

% Ostateczna warto�� prog�w dla ka�dego pikesla to �rednia z prog�w jego 
% otoczenia o promieniu ROUND
ROUND = 4;
THRESHOLDS = zeros(size(T));
for i = 1:1:size(T, 1)
    for j = 1:1:size(T, 2)
        % We� otoczenie piksela
        sub = T(max((i-ROUND), 1):min((i+ROUND), end), max((j-ROUND),1):min((j+ROUND),end));
        % Wyznacz �redni� z prog�w otoczenia
        THRESHOLDS(i,j) = mean2(sub);
    end
end

if (strcmp(debug, 'debug'))
    display(THRESHOLDS);
end;
end


