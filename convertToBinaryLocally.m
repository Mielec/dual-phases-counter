function [ binaryImage ] = convertToBinaryLocally( filename )

% Odczytanie obrazu z podanego pliku
image = imread(filename);

% Konwersja obrazu do skali szaro�ci (Macierz 2D z warto�ciami od 0 do 256)
image = rgb2gray(image);

% Wyznaczenie macierzy lokalnych poziom�w progowania (dla ka�dego piksela)
thresholds = grayThreshLocal(image, 'noDebug');

% Zadeklarowanie wynikowej macierzy binarnej
binaryImage = zeros(size(image));

for i = 1:1:size(image, 1)
    for j = 1:1:size(image, 2)
        % Progowanie ka�dego piksela jego w�asnym poziomem
        binaryImage(i, j) = im2bw(image(i,j), thresholds(i,j));
    end
end

end

