function [ martensitPercentage, ferritPercentage ] = countPhases( binaryImage )

% Obliczenie ilo�ci wszystkich pikseli
pixelsCount = size(binaryImage, 1) * size(binaryImage, 2);

% Obliczenie udzia�u martenzytu (1 oznacza bia�e piksele)
martensitCount = sum(binaryImage(:) == 1);
martensitPercentage = (martensitCount / pixelsCount) * 100;

% Obliczenie udzia�u ferrytu (0 oznacza czarne piksele)
ferritCount = sum(binaryImage(:) == 0);
ferritPercentage = (ferritCount / pixelsCount) * 100;

% Wypisanie wyniku 
fprintf('Ilo�� wszystkich punkt�w:      %d\n',      pixelsCount);
fprintf('Ilo�� punkt�w ferrytu:         %d\n',      ferritCount);
fprintf('Ilo�� punkt�w martenzytu:      %d\n',      martensitCount);
fprintf('Udzia� procentowy ferrytu:     %.2f\n',    ferritPercentage);
fprintf('Udzia� procentowy martenzytu:  %.2f\n',    martensitPercentage);

end

