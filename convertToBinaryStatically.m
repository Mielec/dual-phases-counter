function [ binaryImage ] = convertToBinaryStatically( filename )
% Odczytanie obrazu z podanego pliku
image = imread(filename);
% Konwersja obrazu do skali szarości (Macierz 2D z wartościami od 0 do 256)
image = rgb2gray(image);
% Wyznaczenie globalnego poziomu progowania
threshold = 0.456;
% Konwersja obrazu do postaci binarnej
binaryImage = im2bw(image, threshold);
end


