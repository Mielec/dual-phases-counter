IMAGES = {
    'images/image1.jpg';
    'images/image2.jpg';
    'images/image3.jpg';
    'images/image4.jpg';
    };

METHODS = {
    @convertToBinaryStatically;
    @convertToBinaryGlobally;
    @convertToBinaryLocally;
    };

for m = 1:size(METHODS)
   for i = 1:size(IMAGES)
       method = METHODS{m};
       image = IMAGES{i};
       processImage(image, method);
   end
end