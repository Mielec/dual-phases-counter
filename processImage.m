function [] = processImage( filename , binarizationMethod)    
    binaryImage = binarizationMethod(filename);
    countPhases(binaryImage);
    
    name = filename(1:end-4);
    imwrite(binaryImage, sprintf('%s-%s.jpg',name, func2str(binarizationMethod)));
end

